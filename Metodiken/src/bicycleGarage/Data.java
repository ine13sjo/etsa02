package bicycleGarage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.io.Serializable;

/**
 * This class handles the data of the bicycle garage.
 * 
 * @author ETSA02 Group 03
 * @version 1.0
 */
public class Data implements Serializable {
	private Map<String, BicycleOwner> owners;
	private Map<String, Bicycle> bicycles;
	private ArrayList<Bicycle> inGarage;
	private int garageSize;
	private static final long serialVersionUID = 1L;

	public Data(int size) {
		owners = new HashMap<String, BicycleOwner>();
		bicycles = new HashMap<String, Bicycle>();
		inGarage = new ArrayList<Bicycle>();
		garageSize = size;
	}

	/**
	 * Checks that the personal ID number is valid using the luhn-algoritm.
	 * 
	 * @param persNbr
	 *            The personal ID number to be checked
	 * @return true if the ID number is valid, otherwise false
	 */

	private boolean isTrue(String persNbr) {
		int sum = 0;
		boolean alternate = false;
		for (int i = 9; i >= 0; i--) {
			int n = Integer.parseInt(persNbr.substring(i, i + 1));
			if (alternate) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			sum += n;
			alternate = !alternate;
		}
		return (sum % 10 == 0);
	}

	/**
	 * Adds a bicycle owner to the system by putting it in the map owners.
	 * 
	 * @param name
	 *            The name of the bicycle owner
	 * @param persNbr
	 *            The personal ID number of the bicycle owner
	 * @param phoneNbr
	 *            The phonenumber of the bicycle owner
	 * @return true if the owner was successfully added
	 */
	public String addOwner(String name, String persNbr, String phoneNbr) {
		if (isTrue(persNbr) && !owners.containsKey(persNbr)) {
			owners.put(persNbr, new BicycleOwner(name, persNbr, phoneNbr));
			return owners.get(persNbr).toString();
		} else if (!isTrue(persNbr)) {
			return "Cykel�garen kunde inte l�ggas till d� det uppgivna personnumret �r falskt";
		} else if (owners.containsKey(persNbr)) {
			return "Cykel�garen kunde inte l�ggas till d� hen redan finns registrerad";
		} else {
			return "Cykel�garen kunde inte l�ggas till";
		}
	}

	/**
	 * Adds a bicycle to a owner and generates a barcode. Adds the bicycle to
	 * the map of bicycles.
	 * 
	 * @param persNbr
	 *            The personal ID number of the bicycle owner
	 * @return The barcode of the added bicycle
	 */
	public String addBicycleToOwner(String persNbr) {
		if (owners.containsKey(persNbr)) {
			StringBuilder sb = new StringBuilder();
			Random rand = new Random();
			for (int i = 0; i < 5; i++) {
				sb.append(rand.nextInt(10));
			}
			String barcode = sb.toString();
			while (bicycles.containsKey(barcode)) {
				sb = new StringBuilder();
				rand = new Random();
				for (int i = 0; i < 5; i++) {
					sb.append(rand.nextInt(10));
				}
				barcode = sb.toString();
			}
			if (owners.get(persNbr).addBicycleToOwner(barcode)) {
				bicycles.put(barcode, new Bicycle(barcode));
				return "Cykeln med streckkod " + barcode + " �r nu registrerad";
			} else {
				return "Cykeln kunde inte registreras d� cykel�garen redan har max antal cyklar registrerade";
			}
		}
		return "Cykeln kunde inte registreras d� cykel�garen inte finns registrerad";
	}

	/**
	 * Deletes a specific owner with the specified personal ID number.
	 * 
	 * @param persNbr
	 *            The personal ID number of the bicycle owner to be removed
	 * @return A message containing information about if the bicycle owner was
	 *         removed or why it couldn't be removed
	 */
	public String removeOwner(String persNbr) {
		if (owners.containsKey(persNbr)) {
			BicycleOwner owner = owners.get(persNbr);
			if (owner.nbrOfBikes() == 0) {
				owners.remove(persNbr);
				return "Cykel�garen med personnummer " + persNbr + " �r avregistrerad";
			}
			for (int i = 0; i < owner.nbrOfBikes(); i++) {
				Bicycle b = owner.getBicycle(i);
				if (inGarage.contains(b)) {
					return "Cykel�garen kunde inte avregistreras d� hen har cyklar registrerade i garaget";
				}
				bicycles.remove(b.getBarcode());
			}
			owners.remove(persNbr);
			return "Cykel�garen med personnummer " + persNbr + " och dess cyklar �r avregistrerade";
		}
		return "Cykel�garen kunde inte avregistreras d� hen inte finns registrerad ";
	}

	/**
	 * Finds the owner by using it's personal ID number.
	 * 
	 * @param persNbr
	 *            The personal ID number of the bicycle owner that is to be
	 *            found
	 * @return The bicycle owner with the correct personal ID number
	 */
	public BicycleOwner getOwner(String persNbr) {
		return owners.get(persNbr);
	}

	/**
	 * Deletes a bicycle from the specified owner and from the map bicycles. The
	 * bicycle can not be deleted if it is in the garage.
	 * 
	 * @param persNbr
	 *            The personal ID number of the bicycle owner
	 * @param barcode
	 *            The barcodde of the bicycle to be deleted
	 * @return true if the bicycle was deleted from the owner, otherwise false
	 */
	public String removeBicycleFromOwner(String persNbr, String barcode) {
		if (owners.containsKey(persNbr)) {
			for (int i = 0; i < 3; i++) {
				if (!bicycles.containsKey(barcode)) {
					return "Cykeln kunde inte avregistreras d� streckkoden inte finns registrerad i systemet";
				} else if (inGarage.contains(bicycles.get(barcode))) {
					return "Cykeln kunde inte avregistreras d� den �r registrerad i garaget";
				} else if (owners.get(persNbr).removeBicycle(barcode)) {
					bicycles.remove(barcode);
					return "Cykeln med streckkod " + barcode + " �r nu avregistrerad";
				} else if (!owners.get(persNbr).getBicycle(i).getBarcode().equals(barcode)) {
					return "Cykeln kunde inte avregistreras d� personnummer och streckkod inte matchar";
				}
			}
		}
		return "Cykeln kunde inte avregistreras";
	}

	/**
	 * Deletes a bicycle with a specific barcode from garage by removing it from
	 * the map inGarage.
	 * 
	 * @param barcode
	 *            The barcode of the bicycle to be removed from the garage
	 * @return true if the delete was successful, otherwise false
	 */
	public boolean removeBicycle(String barcode) {
		if (inGarage.remove(bicycles.get(barcode))) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			bicycles.get(barcode).setExitDate(dateFormat.format(date));
			return true;
		}
		return false;
	}

	/**
	 * Adds a bicycle with a specific barcode to the garage.
	 * 
	 * @param barcode
	 *            The barcode of the bicycle to be added in the garage
	 * @return true if the operation was successful, otherwise false
	 */
	public boolean addBicycle(String barcode) {
		if (bicycles.containsKey(barcode)) {
			if (inGarage.add(bicycles.get(barcode))) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				bicycles.get(barcode).setArrivalDate(dateFormat.format(date));
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the covering of the places in the bicycle garage.
	 * 
	 * @return A string with all the bicycles in the garage and their arrival
	 *         dates.
	 */
	public String covering() {
		StringBuilder sb = new StringBuilder();
		for (Bicycle temp : inGarage) {
			sb.append("Cykelns streckkod: " + temp.toString() + ". Ankomstdatum: " + temp.getArrivalDate());
			sb.append("\n");
		}
		return sb.toString();
	}

	/**
	 * Returns the total number of registered bicycles in the system.
	 * 
	 * @return A string with the number of bicycles registered in the system
	 */
	public String nbrBicycles() {
		return String.valueOf(bicycles.size());
	}

	/**
	 * Returns the total number of registered bicycle owners in the system.
	 * 
	 * @return A string with the number of bicycle owners registered in the
	 *         system
	 */
	public String nbrBicycleOwners() {
		return String.valueOf(owners.size());
	}

	/**
	 * Generates a new barcode for a bicycle.
	 * 
	 * @param oldBarcode
	 *            The bicycle's barcode before it is changed
	 * @return The new barcode of the bicycle
	 */
	public String changeBarcode(String oldBarcode) {

		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for (int i = 0; i < 5; i++) {
			sb.append(rand.nextInt(10));
		}
		String barcode = sb.toString();
		while (bicycles.containsKey(barcode)) {
			sb = new StringBuilder();
			rand = new Random();
			for (int i = 0; i < 5; i++) {
				sb.append(rand.nextInt(10));
			}
			barcode = sb.toString();
		}
		for (String p : owners.keySet()) {
			for (int i = 0; i < owners.get(p).nbrOfBikes(); i++) {
				if (oldBarcode.equals(owners.get(p).getBicycle(i).getBarcode())) {
					owners.get(p).getBicycle(i).setBarcode(barcode);
					bicycles.get(oldBarcode).setBarcode(barcode);
					bicycles.remove(oldBarcode);
					bicycles.put(barcode, owners.get(p).getBicycle(i));
				}
			}
		}
		return bicycles.get(barcode).getBarcode();
	}

	/**
	 * Checks if the bicycle is registered in the system.
	 * 
	 * @param s
	 *            The barcode of the bicycle
	 * @return true if the bicycle is in the system
	 */
	public boolean containsBicycle(String s) {
		if (bicycles.containsKey(s)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the bicycle is checked in in the garage.
	 * 
	 * @param s
	 *            The barcode of the bicycle
	 * @return true if the bicycle is in the garage
	 */
	public boolean isCheckedIn(String s) {
		if (inGarage.contains(bicycles.get(s))) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the bicycle garage is full.
	 * 
	 * @return true if the bicycle garage is full
	 */
	public boolean garageIsFull() {
		if (garageSize <= inGarage.size()) {
			return true;
		}
		return false;
	}

	/**
	 * Returns a string with the number of bicycles in the bicycle garage.
	 * 
	 * @return The number of checked in bicycles
	 */
	public String nbrInGarage() {
		return String.valueOf(inGarage.size());
	}

	/**
	 * Checks if the pincode belongs to a bicycle owner.
	 * 
	 * @param pincode
	 *            The pincode you want to check
	 * @return true if the pincode exists
	 */
	public boolean pincodeExists(String pincode) {
		Set<String> temp = owners.keySet();
		for (String s : temp) {
			if (owners.get(s).getPin().equals(pincode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns an ArrayList with the bicycles belonging to the bicycle owner
	 * with a specific pincode.
	 * 
	 * @param pincode
	 *            The pincode of the bicycle owner
	 * @return a list with the bicycles
	 */
	public ArrayList<Bicycle> getOwnersBicycles(String pincode) {
		Set<String> temp = owners.keySet();
		ArrayList<Bicycle> bicycleList = new ArrayList<Bicycle>();
		for (String s : temp) {
			if (owners.get(s).getPin().equals(pincode)) {
				BicycleOwner owner = owners.get(s);
				for (int i = 0; i < owner.nbrOfBikes(); i++) {
					bicycleList.add(owner.getBicycle(i));
				}
			}
		}
		return bicycleList;
	}

	/**
	 * Changes the pincode that belongs to the bicycle owner, the new pincode is
	 * a string that consists of 4 random numbers between 0 and 9.
	 * 
	 * @return The new pincode
	 */

	public String changePin(String persNbr) {
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for (int i = 0; i < 4; i++) {
			sb.append(rand.nextInt(10));
		}
		String pincode = sb.toString();
		for (String s : owners.keySet()) {
			if (owners.get(s).getPin().equals(pincode)) {
				sb = new StringBuilder();
				rand = new Random();
				for (int i = 0; i < 5; i++) {
					sb.append(rand.nextInt(10));
				}
				pincode = sb.toString();				
			}
			owners.get(s).setPin(pincode);
		}
		return owners.get(persNbr).getPin();
	}
}
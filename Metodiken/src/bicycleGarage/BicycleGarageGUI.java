package bicycleGarage;

import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

/**
 * This class creates the user interface of the bicycle garage.
 * 
 * @author ETSA02 Group 03
 * @version 1.0
 */
public class BicycleGarageGUI {
	private HardwareManager hardware;
	private BorderPane root;
	private TextArea textArea;
	private Data data;

	/**
	 * Creates a new BicycleGarage with the hardwareManager.
	 * 
	 * @param hw
	 *            The hardwareManager used to implement the user interface
	 */
	public BicycleGarageGUI(Data data) {
		this.data = data;
		hardware = new HardwareManager(data);
		textArea = new TextArea();
		textArea.setEditable(false);
		textArea.setWrapText(true);
		textArea.setPrefColumnCount(50);
		textArea.setPrefRowCount(20);

		root = new BorderPane();
		root.setTop(makeMenu());
		root.setCenter(textArea);

		Button quit = new Button("Avsluta");
		quit.setOnAction(e -> quit());
		root.setBottom(quit);
	}

	/**
	 * Returns the root of the user interface.
	 * 
	 * @return The root
	 */
	public Parent getRoot() {
		return root;
	}

	private void quit() {
		Platform.exit();
	}

	private MenuBar makeMenu() {
		final Menu menuBicycleOwners = new Menu("Cykel�gare");
		final MenuItem menuRegisterOwner = new MenuItem("Registrera cykel�gare");
		menuRegisterOwner.setOnAction(e -> registerOwner());
		final MenuItem menuUnregisterOwner = new MenuItem("Avregistrera cykel�gare");
		menuUnregisterOwner.setOnAction(e -> unregisterOwner());
		final MenuItem menuChangePin = new MenuItem("�ndra pinkod");
		menuChangePin.setOnAction(e -> changePin());

		menuBicycleOwners.getItems().addAll(menuRegisterOwner, menuUnregisterOwner, menuChangePin);

		final Menu menuBicycles = new Menu("Cyklar");
		final MenuItem menuRegisterBicycle = new MenuItem("Registrera cykel");
		menuRegisterBicycle.setOnAction(e -> registerBicycle());
		final MenuItem menuUnregisterBicycle = new MenuItem("Avregistrera cykel");
		menuUnregisterBicycle.setOnAction(e -> unregisterBicycle());
		final MenuItem menuAddToGarage = new MenuItem("L�gg till cykel i garaget");
		menuAddToGarage.setOnAction(e -> addBicycle());
		final MenuItem menuRemoveFromGarage = new MenuItem("Ta bort cykel ur garaget");
		menuRemoveFromGarage.setOnAction(e -> removeBicycle());
		final MenuItem menuNewBarcode = new MenuItem("Ny streckkod");
		menuNewBarcode.setOnAction(e -> changeBarcode());

		menuBicycles.getItems().addAll(menuRegisterBicycle, menuUnregisterBicycle, menuAddToGarage,
				menuRemoveFromGarage, menuNewBarcode);

		final Menu menuStatistics = new Menu("Statistik");
		final MenuItem menuCovering = new MenuItem("Bel�ggning");
		menuCovering.setOnAction(e -> covering());
		final MenuItem menuUserInfo = new MenuItem("Anv�ndarinfo");
		menuUserInfo.setOnAction(e -> userInfo());
		final MenuItem menuRegBicycleOwners = new MenuItem("Antal cykel�gare");
		menuRegBicycleOwners.setOnAction(e -> nbrBicycleOwners());
		final MenuItem menuRegBicycles = new MenuItem("Antal registrerade cyklar");
		menuRegBicycles.setOnAction(e -> nbrBicycles());

		menuStatistics.getItems().addAll(menuCovering, menuUserInfo, menuRegBicycleOwners, menuRegBicycles);

		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(menuBicycleOwners, menuBicycles, menuStatistics);
		return menuBar;
	}

	private void changeBarcode() {
		Optional<String> result = oneInputDialog("�ndra streckkod", "�ndra streckkod", "Gammal streckkod");
		if (result.isPresent()) {
			try {
				String barcode = result.get();
				textArea.setText("Cykelns nya streckkod �r: " + data.changeBarcode(barcode));
			} catch (ArrayIndexOutOfBoundsException e) {
				textArea.setText("Kunde inte ta bort cykeln ur garaget");
			}
		} else {
			textArea.setText("Kunde inte ta bort cykeln ur garaget");
		}
	}

	private void removeBicycle() {
		Optional<String> result = oneInputDialog("Ta bort cykel ur garaget", "Ta bort cykel ur garaget", "Streckkod");
		if (result.isPresent()) {
			try {
				String barcode = result.get();
				if (data.removeBicycle(barcode)) {
					textArea.setText("Cykeln med streckkod " + barcode + " �r borttagen ur garaget");
				} else {
					textArea.setText("Kunde inte ta bort cykeln ur garaget");
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				textArea.setText("Kunde inte ta bort cykeln ur garaget");
			}
		} else {
			textArea.setText("Kunde inte ta bort cykeln ur garaget");
		}
	}

	private void addBicycle() {
		Optional<String> result = oneInputDialog("L�gg till cykel i garaget", "L�gg till cykel i garaget", "Streckkod");
		if (result.isPresent()) {
			try {
				String barcode = result.get();
				if (!data.garageIsFull()) {
					if (!data.containsBicycle(barcode)) {
						textArea.setText(
								"Kunde inte l�gga till cykeln i garaget d� streckkoden inte finns registrerad");
					} else if (data.isCheckedIn(barcode)) {
						textArea.setText(
								"Kunde inte l�gga till cykeln i garaget d� den redan �r registrerad i garaget");
					} else {
						if (data.addBicycle(barcode)) {
							textArea.setText("Cykeln med streckkod " + barcode + " �r tillagd i garaget");
						}
					}					
				} else {
					textArea.setText("Kunde inte l�gga till cykeln i garaget d� cykelgaraget �r fullt");
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				textArea.setText("Kunde inte l�gga till cykeln i garaget");
			}
		} else {
			textArea.setText("Kunde inte l�gga till cykeln i garaget");
		}
	}

	private void changePin() {
		Optional<String> result = oneInputDialog("�ndra pinkod", "�ndra pinkod", "Personnummer");
		if (result.isPresent()) {
			try {
				String persNbr = result.get();
				data.changePin(persNbr);
				textArea.setText("Den nya pinkoden �r: " + data.getOwner(persNbr).getPin());
			} catch (NullPointerException e) {
				textArea.setText("Det gick inte att �ndra pinkod");
			}
		} else {
			textArea.setText("Det gick inte att �ndra pinkod");
		}
	}

	private void nbrBicycles() {
		textArea.setText("Antal registrerade cyklar: " + data.nbrBicycles());
	}

	private void userInfo() {
		Optional<String> result = oneInputDialog("Anv�ndarinfo", "Anv�ndarinfo", "Personnummer");
		if (result.isPresent()) {
			try {
				String persNbr = result.get();
				textArea.setText(data.getOwner(persNbr).toString());

			} catch (NullPointerException e) {
				textArea.setText("Det gick inte att h�mta information om anv�ndaren");
			}
		} else {
			textArea.setText("Det gick inte att h�mta information om anv�ndaren");
		}
	}

	private void nbrBicycleOwners() {
		textArea.setText("Antal registrerade cykel�gare: " + data.nbrBicycleOwners());
	}

	private void covering() {
		textArea.setText(data.covering());
	}

	private void unregisterBicycle() {
		String[] a = new String[2];
		a[0] = "Personnummer";
		a[1] = "Streckkod";
		Optional<String[]> result = twoInputsDialog("Avregistrera cykel fr�n cykel�gare",
				"Avregistrera cykel fr�n cykel�gare", a);
		if (result.isPresent()) {
			try {
				String[] res = result.get();
				String persNbr = res[0];
				String barcode = res[1];
				textArea.setText(data.removeBicycleFromOwner(persNbr, barcode));
			} catch (ArrayIndexOutOfBoundsException e) {
				textArea.setText("Kunde inte ta bort cykeln");
			}
		} else {
			textArea.setText("Kunde inte ta bort cykeln");
		}
	}

	private void unregisterOwner() {
		Optional<String> result = oneInputDialog("Avegistrera cyckel�gare", "Avregistrera cykel�gare", "Personnummer");
		if (result.isPresent()) {
			try {
				String persNbr = result.get();
				textArea.setText(data.removeOwner(persNbr));

			} catch (NullPointerException e) {
				textArea.setText("Det gick inte att avregistrera cykel�garen");
			}
		} else
			textArea.setText("Det gick inte att avregistrera cykel�garen");
	}

	private void registerBicycle() {
		Optional<String> result = oneInputDialog("Registrera cykel till �gare", "Registrera cykel till �gare",
				"Personnummer");
		if (result.isPresent()) {
			try {
				String persNbr = result.get();
				textArea.setText(data.addBicycleToOwner(persNbr));
			} catch (NullPointerException e) {
				textArea.setText("Det gick inte att l�gga till cykeln");
			}

		} else {
			textArea.setText("Det gick inte att l�gga till cykeln");
		}
	}

	private void registerOwner() {
		String[] a = new String[3];
		a[0] = "Namn";
		a[1] = "Personnummer";
		a[2] = "Telefonnummer";
		Optional<String[]> result = threeInputsDialog("Registrera cykel�gare", "Registrera cykel�gare", a);
		if (result.isPresent()) {
			try {
				String[] res = result.get();
				String name = res[0];
				String persNbr = res[1];
				String phoneNbr = res[2];
				textArea.setText(data.addOwner(name, persNbr, phoneNbr));
			} catch (ArrayIndexOutOfBoundsException e) {
				textArea.setText("Cykel�garen kunde inte l�ggas till");
			}
		} else {
			textArea.setText("Cykel�garen kunde inte l�ggas till");
		}
	}

	private Optional<String> oneInputDialog(String title, String headerText, String label) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle(title);
		dialog.setHeaderText(headerText);
		dialog.setContentText(label);
		Optional<String> result = dialog.showAndWait();
		String input = null;
		if (result.isPresent()) {
			input = result.get();
		}
		return Optional.ofNullable(input);
	}

	private Optional<String[]> twoInputsDialog(String title, String headerText, String[] labels) {
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle(title);
		dialog.setHeaderText(headerText);
		dialog.setResizable(true);
		Label label1 = new Label(labels[0] + ':');
		Label label2 = new Label(labels[1] + ':');
		TextField tf1 = new TextField();
		TextField tf2 = new TextField();
		GridPane grid = new GridPane();
		grid.add(label1, 1, 1);
		grid.add(tf1, 2, 1);
		grid.add(label2, 1, 2);
		grid.add(tf2, 2, 2);
		dialog.getDialogPane().setContent(grid);
		ButtonType buttonTypeOk = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType buttonTypeCancel = new ButtonType("Avbryt", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonTypeCancel, buttonTypeOk);
		dialog.setResultConverter(new Callback<ButtonType, String>() {
			@Override
			public String call(ButtonType b) {
				String inputs = null;
				if (b == buttonTypeOk) {
					inputs = tf1.getText() + ":" + tf2.getText();
				}
				return inputs;
			}
		});
		tf1.requestFocus();
		Optional<String> result = dialog.showAndWait();
		String[] input = null;
		if (result.isPresent()) {
			input = result.get().split(":");
		}
		return Optional.ofNullable(input);
	}

	private Optional<String[]> threeInputsDialog(String title, String headerText, String[] labels) {
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle(title);
		dialog.setHeaderText(headerText);
		dialog.setResizable(true);
		Label label1 = new Label(labels[0] + ':');
		Label label2 = new Label(labels[1] + ':');
		Label label3 = new Label(labels[2] + ':');
		TextField tf1 = new TextField();
		TextField tf2 = new TextField();
		TextField tf3 = new TextField();
		GridPane grid = new GridPane();
		grid.add(label1, 1, 1);
		grid.add(tf1, 2, 1);
		grid.add(label2, 1, 2);
		grid.add(tf2, 2, 2);
		grid.add(label3, 1, 3);
		grid.add(tf3, 2, 3);
		dialog.getDialogPane().setContent(grid);
		ButtonType buttonTypeOk = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType buttonTypeCancel = new ButtonType("Avbryt", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonTypeCancel, buttonTypeOk);
		dialog.setResultConverter(new Callback<ButtonType, String>() {
			@Override
			public String call(ButtonType b) {
				String inputs = null;
				if (b == buttonTypeOk) {
					inputs = tf1.getText() + ":" + tf2.getText() + ":" + tf3.getText();
				}
				return inputs;
			}
		});
		tf1.requestFocus();
		Optional<String> result = dialog.showAndWait();
		String[] input = null;
		if (result.isPresent()) {
			input = result.get().split(":");
		}
		return Optional.ofNullable(input);
	}
}

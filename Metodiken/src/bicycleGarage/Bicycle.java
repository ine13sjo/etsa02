package bicycleGarage;

import java.io.Serializable;
import java.util.Random;

/**
 * This class represents a bicycle.
 * 
 * @author ETSA02 Group 03
 * @version 1.0
 */
public class Bicycle implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String barcode;
	private String arrivalDate;
	private String exitDate;

	/**
	 * Creates a new bicycle with a specific barcode and sets the arrivaldate to
	 * null.
	 * 
	 * @param barcode
	 *            The barcode that will be assigned to the bicycle
	 */
	public Bicycle(String barcode) {
		this.barcode = barcode;
		arrivalDate = null;
		exitDate = null;
	}

	/**
	 * Returns the bicycle's barcode.
	 * 
	 * @return The barcode belonging to the bicycle
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * Sets the barcode to a specific string containing 5 numbers between 0 and
	 * 9.
	 * 
	 * @param barcode
	 *            The bicycle's new barcode
	 * @return The new barcode belonging to the bicycle
	 */
	public String setBarcode(String barcode) {
		this.barcode = barcode;
		return barcode;
	}

	/**
	 * Returns the bicycle's barcode.
	 * 
	 * @return The barcode belonging to the bicycle
	 */
	public String toString() {
		return barcode;
	}

	/**
	 * Sets the bicycle's arrivalDate to the string arrivaldate.
	 * 
	 * @param arrivalDate
	 *            The arrivaldate of the bicycle
	 */
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * Returns the bicycle's arrivaldate.
	 * 
	 * @return The arrivaldate of the bicycle.
	 */
	public String getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * Sets the bicycle's exitDate to the string exitdate.
	 * 
	 * @param exitDate
	 *            The exitdate of the bicycle
	 */
	public void setExitDate(String exitDate) {
		this.exitDate = exitDate;
	}

	/**
	 * Returns the bicycle's exitdate.
	 * 
	 * @return The exitdate of the bicycle.
	 */
	public String getExitDate() {
		return exitDate;
	}

	/**
	 * Checks if the bicycle objects are equal
	 * 
	 * @param b
	 *            The bicycle to be compared
	 * @return true if the barcodes are the same
	 */
	public boolean equals(Object b) {
		if (b instanceof Bicycle) {
			return barcode.equals(((Bicycle) b).barcode);
		} else {
			return false;
		}
	}
}

package bicycleGarage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Optional;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


/**
 * This class creates and runs the application of the bicycle garage.
 * 
 * @author ETSA02 Group 03
 * @version 1.0
 */
public class BicycleGarageApplication extends Application implements Serializable {
	private static final long serialVersionUID = 1L;
	private Data data;
	private Stage stage; 

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage; 
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.setInitialFileName("garage");
		File selectedFile = fileChooser.showOpenDialog(stage);
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(selectedFile));
			data = (Data) in.readObject();
			in.close();
			
		} 
		catch(NullPointerException e) {
			TextInputDialog dialog = new TextInputDialog();
			dialog.setTitle("Garagestorlek");
			dialog.setHeaderText("");
			dialog.setContentText("Storlek p� garaget:");
			Optional<String> result = dialog.showAndWait();
			String input = null;
			if (result.isPresent()) {
				input = result.get();
			}
			data = new Data(Integer.parseInt(input));			
		}
		catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
		BicycleGarageGUI gui = new BicycleGarageGUI(data);
		Scene scene = new Scene(gui.getRoot());
		stage.setScene(scene);
		stage.setTitle("Huvudmeny");
		stage.show();
		stage.setTitle("Bicycle Garage");
		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void stop() {
		 FileChooser fileChooser = new FileChooser();
		 fileChooser.setTitle("Save Resource File");
		 fileChooser.setInitialFileName("garage");
		 File selectedFile = fileChooser.showSaveDialog(stage);
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(selectedFile));
			out.writeObject(data);
			out.close();
		} catch (NullPointerException e){
			System.exit(1);
		}catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
}

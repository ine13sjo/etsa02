package bicycleGarage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class represents a bicycle owner.
 * 
 * @author ETSA02 Group 03
 * @version 1.0
 */
public class BicycleOwner implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String persNbr;
	private String pin;
	private String phoneNbr;
	private ArrayList<Bicycle> bicycles;

	/**
	 * Constructs a new bicycle owner and assigns it a pincode.
	 * 
	 * @param name
	 *            The name of the bicycle owner
	 * @param persNbr
	 *            The personal ID number of the bicycle owner
	 * @param teleNbr
	 *            The phonenumber of the bicycle owner
	 */
	public BicycleOwner(String name, String persNbr, String teleNbr) {
		this.name = name;
		this.persNbr = persNbr;
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for (int i = 0; i < 4; i++) {
			sb.append(rand.nextInt(10));
		}
		pin = sb.toString();
		this.phoneNbr = teleNbr;
		bicycles = new ArrayList<Bicycle>();
	}

	/**
	 * Returns the name of the bicycle owner.
	 * 
	 * @return The name of the bicycle owner
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the bicycleowner's pincode which is a string containing four
	 * numbers.
	 * 
	 * @return The pincode of the bicycle owner
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * Retrieves the bicycleowner's phone number which is a string containing 10
	 * numbers.
	 * 
	 * @return The phonenumber of the bicycle owner
	 */
	public String getPhoneNbr() {
		return phoneNbr;
	}

	/**
	 * Retrieves the bicycle owner's personal ID number which is a string
	 * containing 10 numbers.
	 * 
	 * @return The bicycle owner's personal ID number
	 */
	public String getPersNbr() {
		return persNbr;
	}

	/**
	 * Sets the pincode to a new string that consists of four numbers between 0
	 * and 9.
	 * 
	 * @param pin
	 *            The pincode to be set
	 * @return The pincode that is set
	 */
	public String setPin(String pin) {
		this.pin = pin;
		return pin;
	}

	/**
	 * Prints the bicycleowner's name, personal ID, phonenumber and pincode.
	 * 
	 * @return A string with the bicycleowner's name, personal ID number,
	 *         phonenumber, pincode and its bicycle's barcodes
	 */
	public String toString() {
		return "Namn: " + name + ". Personnummer: " + persNbr + ". Telefonnummer: " + phoneNbr + "\n" + "Pinkod: " + pin
				+ ". Cyklarnas streckkoder �r: " + bicycles.toString();
	}

	/**
	 * Adds a new bicycle to the bicycle owner.
	 * 
	 * @return true if the bicycle is added, otherwise false
	 */
	public boolean addBicycleToOwner(String barcode) {
		if (bicycles.size() > 3) {
			return false;
		} else {
			Bicycle b = new Bicycle(barcode);
			for (int i = 0; i < 3; i++) {
				if (bicycles.size() == i) {
					bicycles.add(i, b);
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Returns the bicycle from a specific index in the ArrayList containing all
	 * the bicycles.
	 * 
	 * @param index
	 *            The index of the ArrayList
	 * @return The bicycle on the specific index of the ArrayList
	 */
	public Bicycle getBicycle(int index) {
		return bicycles.get(index);
	}

	/**
	 * Returns the total number of bicycles in the ArrayList of the bicycle
	 * owner.
	 * 
	 * @return The total number of bikes belonging to the bicycle owner
	 */
	public int nbrOfBikes() {
		return bicycles.size();
	}

	/**
	 * Removes the bicycle with the correct barcode.
	 * 
	 * @param barcode
	 *            The barcode belonging to the bicycle to be removed
	 * @return true if the bicycle is removed from the list, otherwise false
	 */
	public boolean removeBicycle(String barcode) {
		for (Bicycle b : bicycles) {
			if (b.getBarcode().equals(barcode)) {
				bicycles.remove(b);
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds the personal ID number using the pincode
	 * 
	 * @param pincode
	 *            The pincode of the bicycle owner to be find
	 * @return personal ID number of the owner with the pincode
	 */
	public String getPersNbr(String pincode) {
		if (pincode.equals(pin)) {
			return persNbr;
		} else {
			return null;
		}
	}
}

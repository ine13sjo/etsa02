package bicycleGarage;
import java.io.Serializable;
import java.util.ArrayList;
import hardware_interfaces.*;
import hardware_testdrivers.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

/**
 * This class controls the hardware of the bicycle garage.
 * 
 * @author ETSA02 Group 03
 * @version 1.0
 */
public class HardwareManager implements Serializable {
	private static final long serialVersionUID = 1L;
	private BarcodeScanner entryScanner;
	private BarcodeScanner exitScanner;
	private PincodeTerminalTestDriver pincode;
	private Data data;
	private ElectronicLockTestDriver doorA;
	private ElectronicLockTestDriver doorB;

	/**
	 * Constructs a new HardwareManager. It creates maps of BicycleOwners,
	 * Bicycles and Bicycles in the garage as well as opening the simulated
	 * hardware.
	 * 
	 */
	public HardwareManager(Data data) {
		this.data = data;
		entryScanner = new BarcodeScannerTestDriver("Ingångsskanner", 0, 0);
		entryScanner.registerObserver(new EntryBarcodeObserver());
		exitScanner = new BarcodeScannerTestDriver("Utgångsskanner", 0, 150);
		exitScanner.registerObserver(new ExitBarcodeObserver());
		pincode = new PincodeTerminalTestDriver("Pinkodsterminal", 0, 300);
		pincode.registerObserver(new PincodeHandler());
		doorA = new ElectronicLockTestDriver("doorA", 0, 600);
		doorB = new ElectronicLockTestDriver("doorB", 300, 600);
	}

	private class EntryBarcodeObserver implements BarcodeObserver {
		@Override
		public void handleBarcode(String s) {
			if (!data.garageIsFull()) {
				if (data.containsBicycle(s) && !data.isCheckedIn(s)) {
					if (data.addBicycle(s)) {
						doorA.open(15);
						pincode.lightLED(PincodeTerminal.GREEN_LED, 15);
					}
				} else if (!data.containsBicycle(s)) {
					pincode.lightLED(PincodeTerminal.RED_LED, 1);
					Timeline timeline = new Timeline(
							new KeyFrame(Duration.millis(2000), e -> pincode.lightLED(PincodeTerminal.RED_LED, 1)));
					timeline.setCycleCount(2);
					timeline.play();
				} else if (data.isCheckedIn(s)) {
					data.removeBicycle(s);
					if (data.addBicycle(s)) {
						doorA.open(15);
						pincode.lightLED(PincodeTerminal.GREEN_LED, 15);
					}
				}
			} else {
				if (data.isCheckedIn(s)) {
					data.removeBicycle(s);
					if (data.addBicycle(s)) {
						doorA.open(15);
						pincode.lightLED(PincodeTerminal.GREEN_LED, 15);
					}
				} else {
					pincode.lightLED(PincodeTerminal.RED_LED, 10);
				}
			}
		}
	}

	private class ExitBarcodeObserver implements BarcodeObserver {
		@Override
		public void handleBarcode(String s) {
			if (data.isCheckedIn(s)) {
				data.removeBicycle(s);
				doorB.open(15);
			} else {
				pincode.lightLED(PincodeTerminal.RED_LED, 1);
				Timeline timeline = new Timeline(
						new KeyFrame(Duration.millis(2000), e -> pincode.lightLED(PincodeTerminal.RED_LED, 1)));
				timeline.setCycleCount(2);
				timeline.play();
			}
		}
	}

	private class PincodeHandler implements PincodeObserver {

		private StringBuilder sb = new StringBuilder();

		@Override
		public void handleCharacter(char c) {
			sb.append(c);
			if (sb.length() == 1) {
				Timeline timeline = new Timeline(new KeyFrame(Duration.millis(5000), e -> sb = new StringBuilder()));
				timeline.play();
			}
			if (sb.length() == 4) {
				String pin = sb.toString();
				sb = new StringBuilder();
				if (data.pincodeExists(pin)) {
					ArrayList<Bicycle> temp = data.getOwnersBicycles(pin);
					for (int i = 0; i < temp.size(); i++) {
						if (data.isCheckedIn(temp.get(i).getBarcode())) {
							pincode.lightLED(PincodeTerminal.GREEN_LED, 15);
							doorA.open(15);
							break;
						} else {
							pincode.lightLED(PincodeTerminal.RED_LED, 1);
							Timeline timeline = new Timeline(new KeyFrame(Duration.millis(2000),
									e -> pincode.lightLED(PincodeTerminal.RED_LED, 1)));
							timeline.setCycleCount(2);
							timeline.play();
						}
					}
				} else if (!data.pincodeExists(pin)) {
					pincode.lightLED(PincodeTerminal.RED_LED, 1);
					Timeline timeline = new Timeline(
							new KeyFrame(Duration.millis(2000), e -> pincode.lightLED(PincodeTerminal.RED_LED, 1)));
					timeline.setCycleCount(2);
					timeline.play();
				} else {
					sb = new StringBuilder();
				}
			} else if (sb.toString().contains("*") || sb.toString().contains("#")) {
				pincode.lightLED(PincodeTerminal.RED_LED, 1);
				Timeline timeline = new Timeline(
						new KeyFrame(Duration.millis(2000), e -> pincode.lightLED(PincodeTerminal.RED_LED, 1)));
				timeline.setCycleCount(2);
				timeline.play();
				sb = new StringBuilder();
			}
		}
	}
}
